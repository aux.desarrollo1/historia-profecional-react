// import React, { useState } from "react";
// import {
//   Formulario,
//   Label,
//   ContenedorTerminos,
//   ContenedorBotonCentrado,
//   Boton,
//   MensajeExito,
//   MensajeError,
//   Formulario1,
// } from "../elementos/Formularios";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
// import Input from "../componentes/Input";

// const App = () => {
//   const [Paterno, Apellido_Paterno] = useState({ campo: "", valido: null });
//   const [Materno, Apellido_Materno] = useState({ campo: "", valido: null });
//   const [Nombre, Nombre_Nombre] = useState({ campo: "", valido: null });
//   const [Domicilio, Domicilio_Domicilio] = useState({ campo: "", valido: null });
//   const [Ciudad, Ciudad_Ciudad] = useState({ campo: "", valido: null });
//   const [Estado , Estado_Estado] = useState({ campo: "", valido: null });
//   const [Codigo, Codigo_Postal] = useState({ campo: "", valido: null });
//   const [Telefono , Telefono_Telefono] = useState({ campo: "", valido: null });
//   const [Correo, Correo_Correo] = useState({ campo: "", valido: null });
//   const [Fecha, Fecha_Nacimiento] = useState({ campo: "", valido: null });
//   const [Civil, Estado_Civil] = useState({ campo: "", valido: null });
//   const [Economia, Expectativa_Economica] = useState({ campo: "", valido: null });

  
//   const expresiones = {
//     nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
//     correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
//     telefono: /^\d{7,14}$/, // 7 a 14 numeros.
//   };
//   const onSubmit = (e) => {
//     e.preventDefault();
//   };

//   return (
	  
//     <main>
// 		<Formulario1>
// 		  <h2>Datos Personales</h2>
// 		</Formulario1>
		
		 
//       <Formulario action="" onSubmit={onSubmit}>
        
// 		<Input
//           estado={Paterno}
//           cambiarEstado={Apellido_Paterno}
//           tipo="text"
//           label="Apellido Paterno:"
//           placeholder="Apellido Paterno"
//           name="Apellido_Paterno"
//           leyendaError="El apellido solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />

    
//         <Input
//           estado={Materno}
//           cambiarEstado={Apellido_Materno}
//           tipo="text"
//           label="Apellido Materno:"
//           placeholder="Apellido Materno"
//           name="Apellido_Materno"
//           leyendaError="El apellido solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />
//         <Input
//           estado={Nombre}
//           cambiarEstado={Nombre_Nombre}
//           tipo="text"
//           label="Nombre:"
//           placeholder="Nombre"
//           name="Nombre"
//           leyendaError="El nombre solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />
//         <Input
//           estado={Domicilio}
//           cambiarEstado={Domicilio_Domicilio}
//           tipo="text"
//           label="Dirección De Domicilio:"
//           placeholder="Dirección De Domicilio"
//           name="Dirección_De_Domicilio"
//           leyendaError="La dirección de domicilio solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />
//         <Input
//           estado={Ciudad}
//           cambiarEstado={Ciudad_Ciudad}
//           tipo="text"
//           label="Ciudad:"
//           placeholder="Ciudad"
//           name="Ciudad"
//           leyendaError="La ciudad solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />
//         <Input
//           estado={Estado}
//           cambiarEstado={Estado_Estado}
//           tipo="text"
//           label="Estado:"
//           placeholder="Estado"
//           name="Estado"
//           leyendaError="El estado solo puede contener letras y espacios."
//           expresionRegular={expresiones.nombre}
//         />
//         <Input
//           estado={Codigo}
//           cambiarEstado={Codigo_Postal}
//           tipo="text"
//           label="Código Postal:"
//           placeholder="Código Postal"
//           name="Codigo_Postal"
//           leyendaError="El codigo postal solo puede contener Numeros y espacios."
//           expresionRegular={expresiones.telefono}
//         />
//         <Input
//           estado={Telefono}
//           cambiarEstado={Telefono_Telefono}
//           tipo="text"
//           label="Número Telefónico:"
//           placeholder="Número Telefónico O Celular"
//           name="Telefono"
//           leyendaError="El telefono solo puede contener numeros."
//           expresionRegular={expresiones.telefono}
//         />

//         <Input
//           estado={Correo}
//           cambiarEstado={Correo_Correo}
//           tipo="email"
//           label="Correo Electrónico:"
//           placeholder="Correo@correo.com"
//           name="correo"
//           leyendaError="El correo solo puede contener letras, numeros, puntos, guiones y guion bajo."
//           expresionRegular={expresiones.correo}
//         />
// 		 <Input
//           estado={Fecha}
//           cambiarEstado={Fecha_Nacimiento}
//           tipo="date"
//           label="Fecha De Nacimiento:"
//           placeholder="Fecha De Nacimiento"
//           name="Fecha_Nacimiento"
//           leyendaError="La fecha de nacimiento solo puede contener numeros."
//           expresionRegular={expresiones.nombre}
//         />
// 		 <Input
//           estado={Civil}
//           cambiarEstado={Estado_Civil}
//           tipo="text"
//           label="Estado Civil:"
//           placeholder="Estado Civil"
//           name="Estado_Civil"
//           leyendaError="El estado civil solo puede contener letras."
//           expresionRegular={expresiones.nombre}
//         />
// 	   <Input
//           estado={Economia}
//           cambiarEstado={Expectativa_Economica}
//           tipo="text"
//           label="Expectativa Economica:"
//           placeholder="Expectativa Economica"
//           name="Expectativa_Economica"
//           leyendaError="La expectativa economica solo puede contener numeros."
//           expresionRegular={expresiones.nombre}
//         />
//        <ContenedorBotonCentrado>
// 					<Boton type="submit">Siguiente</Boton>
// 				</ContenedorBotonCentrado> 
//       </Formulario>
//     </main>
//   );
// };

// export default App;
