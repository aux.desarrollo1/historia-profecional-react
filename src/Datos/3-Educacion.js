// import React, { useState } from "react";
// import {
//     Formulario,
//     Label,
//     ContenedorTerminos,
//     ContenedorBotonCentrado,
//     Boton,
//     MensajeExito,
//     MensajeError,
//     Formulario1,
// } from "../elementos/Formularios";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { faExclamationTriangle } from "@fortawesome/free-solid-svg-icons";
// import Input from "../componentes/Input";

// const App = () => {
//     const [Nombre, Nombre_Nombre] = useState({ campo: "", valido: null });
//     const [Ubicacion, Ubicacion_Ubicacion] = useState({ campo: "", valido: null });
//     const [FechaInicio, Fecha_Inicio] = useState({ campo: "", valido: null });
//     const [FechaFinal, Fecha_Final] = useState({ campo: "", valido: null });
//     const [Grados, Grados_Grados] = useState({ campo: "", valido: null });
//     const [Promedio, Promedio_Promedio] = useState({ campo: "", valido: null });
//     const [Creditos, Creditos_Escolates] = useState({ campo: "", valido: null });
//     const [Actividades, Actividades_Extracurriculares] = useState({ campo: "", valido: null });
//     const [Trabajo, Trabajo_Trabajo] = useState({ campo: "", valido: null });
//     const [NombreUni, Nombre_NombreUni] = useState({ campo: "", valido: null });
//     const [UbicacionUni, Ubicacion_UbicacionUni] = useState({ campo: "", valido: null });
//     const [FechaInicioUni, Fecha_InicioUni] = useState({ campo: "", valido: null });
//     const [FechaFinalUni, Fecha_FinalUni] = useState({ campo: "", valido: null });
//     const [GradosUni, Grados_GradosUni] = useState({ campo: "", valido: null });
//     const [PromedioUni, Promedio_PromedioUni] = useState({ campo: "", valido: null });
//     const [CreditosUni, Creditos_EscolatesUni] = useState({ campo: "", valido: null });
//     const [ActividadesUni, Actividades_ExtracurricularesUni] = useState({ campo: "", valido: null });
//     const [ClaseMas, Clase_Mas] = useState({ campo: "", valido: null });
//     const [ClaseMenos, Clase_Menos] = useState({ campo: "", valido: null });
//     const [Porque, Porque_Porque] = useState({ campo: "", valido: null });
//     const [Financio, Financio_Educacion] = useState({ campo: "", valido: null });
//     const [TrabajoUni, Trabajo_TrabajoUni] = useState({ campo: "", valido: null });
//     const [OtrosCursosUni, Otros_CursosUni] = useState({ campo: "", valido: null });

//     const expresiones = {
//         nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
//         correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
//         telefono: /^\d{7,14}$/, // 7 a 14 numeros.
//     };
//     const onSubmit = (e) => {
//         e.preventDefault();
//     };

//     return (

//         <main>
//             <Formulario1>
//                 <h2>Educacion</h2>
//             </Formulario1>


//             <Formulario action="" onSubmit={onSubmit}>
//                 <Input
//                     estado={Nombre}
//                     cambiarEstado={Nombre_Nombre}
//                     tipo="text"
//                     label="Preparatoria:"
//                     placeholder="Nombre de la Preparatoria"
//                     name="Preparatoria"
//                     leyendaError="La empresa  solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={Ubicacion}
//                     cambiarEstado={Ubicacion_Ubicacion}
//                     tipo="text"
//                     label="Ubicacion:"
//                     placeholder="Ubicacion de la Preparatoria"
//                     name="Ubicacion"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />


//                 <Input
//                     estado={FechaInicio}
//                     cambiarEstado={Fecha_Inicio}
//                     tipo="date"
//                     label="Fecha de Inicio:"
//                     placeholder="Fecha De Inicio De Ciclo Escolar"
//                     name="FechaInicio"
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={FechaFinal}
//                     cambiarEstado={Fecha_Final}
//                     tipo="date"
//                     label="Fecha De Fin:"
//                     placeholder="Fecha de Fin Ciclo Escolar"
//                     name="FechaFinal"
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={Grados}
//                     cambiarEstado={Grados_Grados}
//                     tipo="text"
//                     label="Grados:"
//                     placeholder="Nivel De Grado Mas Alto"
//                     name="Grados"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={Promedio}
//                     cambiarEstado={Promedio_Promedio}
//                     tipo="text"
//                     label="Promedio:"
//                     placeholder="Promedio General "
//                     name="Promedio"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={Creditos}
//                     cambiarEstado={Creditos_Escolates}
//                     tipo="text"
//                     label="Crédito Escolar:"
//                     placeholder="Créditos Escolares"
//                     name="Creditos"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={Actividades}
//                     cambiarEstado={Actividades_Extracurriculares}
//                     tipo="text"
//                     label="Actividades Extracurriculares:"
//                     placeholder="Actividades Extracurriculares"
//                     name="Actividades"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={Trabajo}
//                     cambiarEstado={Trabajo_Trabajo}
//                     tipo="text"
//                     label="¿Trabajo De Medio Tiempo O Durante El Verano?"
//                     placeholder="Nombre del Trabajo"
//                     name="Trabajo"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={NombreUni}
//                     cambiarEstado={Nombre_NombreUni}
//                     tipo="text"
//                     label="Universidad:"
//                     placeholder="Nombre de la Univercidad"
//                     name="Universidad"
//                     leyendaError="La solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={UbicacionUni}
//                     cambiarEstado={Ubicacion_UbicacionUni}
//                     tipo="text"
//                     label="Ubicacion:"
//                     placeholder="Ubicacion de la Universidad"
//                     name="UbicacionUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />


//                 <Input
//                     estado={FechaInicioUni}
//                     cambiarEstado={Fecha_InicioUni}
//                     tipo="date"
//                     label="Fecha de Inicio:"
//                     placeholder="Fecha De Inicio De Ciclo Escolar"
//                     name="FechaInicioUni"
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={FechaFinalUni}
//                     cambiarEstado={Fecha_FinalUni}
//                     tipo="date"
//                     label="Fecha De Fin:"
//                     placeholder="Fecha de Fin Ciclo Escolar"
//                     name="FechaFinalUni"
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={GradosUni}
//                     cambiarEstado={Grados_GradosUni}
//                     tipo="text"
//                     label="Grados:"
//                     placeholder="Nivel De Grado Mas Alto"
//                     name="GradosUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={PromedioUni}
//                     cambiarEstado={Promedio_PromedioUni}
//                     tipo="text"
//                     label="Promedio:"
//                     placeholder="Promedio General"
//                     name="PromedioUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={CreditosUni}
//                     cambiarEstado={Creditos_EscolatesUni}
//                     tipo="text"
//                     label="Crédito Escolar:"
//                     placeholder="Créditos Escolares"
//                     name="CreditosUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={ActividadesUni}
//                     cambiarEstado={Actividades_ExtracurricularesUni}
//                     tipo="text"
//                     label="Actividades Extracurriculares:"
//                     placeholder="Actividades Extracurriculares"
//                     name="ActividadesUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />

//                 <Input
//                     estado={ClaseMas}
//                     cambiarEstado={Clase_Mas}
//                     tipo="text"
//                     label="¿Qué Clases De Su Carrera Disfrutaba Más?"
//                     placeholder="Materia que disfuto mas"
//                     name="ClaseUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={ClaseMenos}
//                     cambiarEstado={Clase_Menos}
//                     tipo="text"
//                     label="¿Qué Clases De Su Carrera No Le Gustaban?"
//                     placeholder="Materia que menos le gustaba"
//                     name="ClaseMenos"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={Porque}
//                     cambiarEstado={Porque_Porque}
//                     tipo="text"
//                     label="¿Por Qúe?"
//                     placeholder="Que fue lo que lo ocaciona"
//                     name="Porque"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={Financio}
//                     cambiarEstado={Financio_Educacion}
//                     tipo="text"
//                     label="¿Cómo Financió Su Educación?"
//                     placeholder="Cómo Financió Su Educación"
//                     name="Finacio"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={TrabajoUni}
//                     cambiarEstado={Trabajo_TrabajoUni}
//                     tipo="text"
//                     label="¿Trabajo Medio Tiempo O Durante El Verano?"
//                     placeholder="Nombre del trabajo"
//                     name="TrabajoUni"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <Input
//                     estado={OtrosCursosUni}
//                     cambiarEstado={Otros_CursosUni}
//                     tipo="text"
//                     label="Otros Cursos, Seminario O Estudios"
//                     placeholder="Cursos, Seminario O Estudios"
//                     name="OtrosCursos"
//                     leyendaError="solo puede contener letras, numeros."
//                     expresionRegular={expresiones.nombre}
//                 />
//                 <ContenedorBotonCentrado>
//                     <Boton type="submit">Anterior</Boton>
//                     <br></br>
//                     <Boton type="submit">Siguiente</Boton>
//                 </ContenedorBotonCentrado>
//             </Formulario>
//         </main>
//     );
// };

// export default App;
