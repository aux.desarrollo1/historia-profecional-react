import React from "react";
import DatosPersonales from "./elementos/DatosPersonales";
import ExperienciaLaboral from "./elementos/ExperenciaLaboral";
import Educacion from "./elementos/Educacion";
import Preguntas from "./elementos/Preguntas";
import Error from "./elementos/PaginaError"
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";



function App() {
  return (

    <Router>
      <Switch>
          <Route exact path="/" component={DatosPersonales} />
          <Route exact path="/pagina1" component={DatosPersonales} />
          <Route exact path="/pagina2" component={ExperienciaLaboral} />
          <Route exact path="/pagina3" component={Educacion} />
          <Route exact path="/pagina4" component={Preguntas} />
          <Route path="*" component={Error} />
      </Switch>
    </Router>

  );
}

export default App;
