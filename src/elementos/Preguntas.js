import { useForm } from "../componentes/validacionfinal";
import React from "react";
import {
    FormularioPreguntas,
    Label,
    Link,
    ContenedorBotonCentrado,
    Boton,
    Formulario1,
    InputRadio,
} from "../elementos/Formularios2";

const initialForm = {
    Pregunta1: "",
    Pregunta2: "",
    Pregunta3: "",
    // Pregunta4: "",
    // Pregunta5: "",
    // Pregunta6: "",
    // Pregunta7: "",
    // Pregunta8: "",
    // Pregunta9: "",
    // Pregunta10: "",
    // Pregunta11: "",
    // Pregunta12: "",
    // Pregunta13: "",
    // Pregunta14: "",
    // Pregunta15: "",
    // Pregunta16: "",
    // Pregunta17: "",
    // Pregunta18: "",
    // Pregunta19: "",
    // Pregunta20: "",
    // Pregunta21: "",
    // Pregunta22: "",
    // Pregunta23: "",
    // Pregunta24: "",
    // Pregunta25: "",
    // Pregunta26: "",
    // Pregunta27: "",
    // Pregunta28: "",
    // Pregunta29: "",
    // Pregunta30: "",
    // Pregunta31: "",
    // Pregunta32: "",
    // Pregunta33: "",
    // Pregunta34: "",
    // Pregunta35: "",
    // Pregunta36: "",
    // Pregunta37: "",
    // Pregunta38: "",
    // Pregunta39: "",
    // Pregunta40: "",
    // Pregunta41: "",
    // Pregunta42: "",
    // Pregunta43: "",
    // Pregunta44: "",
    // Pregunta45: "",
    // Pregunta46: "",
    // Pregunta47: ""
};

const validationsForm = (Formulario) => {
};



const Preguntas = () => {
    const {
        form,
        // errors,
        // loading,
        // response,
        handleChange,
        handleBlur,
        handleSubmit,
    } = useForm(initialForm, validationsForm);
    return (
        <>

            <Formulario1>
                <h2>Preguntas</h2>
            </Formulario1>

            <FormularioPreguntas onSubmit={handleSubmit}>
                <div>
                    <hr></hr>



                    <Label>1- Inteligencia:</Label>
                    <InputRadio
                        type="radio"
                        id="5"
                        name="Pregunta1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />

                    <label>Excelente</label>
                    <InputRadio
                        type="radio"
                        id="4"
                        name="Pregunta1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    <label>Muy Bien</label>
                    <InputRadio
                        type="radio"
                        id="3"
                        name="Pregunta1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    <label>Bien</label>
                    <InputRadio
                        type="radio"
                        id="2"
                        name="Pregunta1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    <label>Suficiente</label>
                    <InputRadio
                        type="radio"
                        id="1"
                        name="Pregunta1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    <label >Mal</label>
                    <Label>Capacidad para entender las cosas y absorber información rápidamente. aprende rápido.</Label>

                    <hr></hr>




                    <Label>2- Capacidad De Análisis:</Label>
                    <InputRadio
                        type="radio"
                        id="5"
                        name="Pregunta2"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    <label>Excelente</label>
                    <InputRadio
                        type="radio"
                        id="4"
                        name="Pregunta2"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Muy Bien</label>
                    <InputRadio
                        type="radio"
                        id="3"
                        name="Pregunta2"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Bien</label>
                    <InputRadio
                        type="radio"
                        id="2"
                        name="Pregunta2"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Suficiente</label>
                    <InputRadio
                        type="radio"
                        id="1"
                        name="Pregunta2"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label >Mal</label>
                    <Label>Identifica los problemas y las oportunidades importantes.
                    analiza a fondo los problemas y a las personas.sabe elegir bien y puede determinar las causas.</Label>

                    <hr></hr>




                    <Label>3- Criterio/Toma De Decisiones:</Label>
                    <InputRadio
                        type="radio"
                        id="5"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Excelente</label>
                    <InputRadio
                        type="radio"
                        id="4"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Muy Bien</label>
                    <InputRadio
                        type="radio"
                        id="3"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Bien</label>
                    <InputRadio
                        type="radio"
                        id="2"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label>Suficiente</label>
                    <InputRadio
                        type="radio"
                        id="1"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <label >Mal</label>
                    <Label>Aplica la lógica,es racional y objetivo al tomar decisiones. no es indeciso,pero tampoco decide en forma impulsiva.</Label>

                    <hr></hr>
                    

                </div>
                <ContenedorBotonCentrado>
                    <Link className="Anterior" to="/pagina3">Anterior</Link>
                    <Boton type="submit" value="Enviar">Enviar</Boton>
                </ContenedorBotonCentrado>

            </FormularioPreguntas>


        </>
    );
};

export default Preguntas;


// {/* 


//                     <Label>4- Capacidad Conceptual:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta4"
//                         Value={form.Pregunta4}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta4"
//                         Value={form.Pregunta4}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta4"
//                         Value={form.Pregunta4}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta4"
//                         Value={form.Pregunta4}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta4"
//                         Value={form.Pregunta4}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Maneja con eficacia no sólo los problemas concretos,
//                     tangibles, sino también las cuestiones abstractas y conceptuales.</Label>

//                     <hr></hr>








//                     <Label>5- Creatividad:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta5"
//                         Value={form.Pregunta5}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta5"
//                         Value={form.Pregunta5}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta5"
//                         Value={form.Pregunta5}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta5"
//                         Value={form.Pregunta5}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta5"
//                         Value={form.Pregunta5}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label> Genera nuevos enfoques para los problemas
//                     o innovaciones para establecer mejores prácticas. muestra imaginación.</Label>

//                     <hr></hr>




//                     <Label>6- Habilidades Estratégicas:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta6"
//                         Value={form.Pregunta6}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta6"
//                         Value={form.Pregunta6}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta6"
//                         Value={form.Pregunta6}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta6"
//                         Value={form.Pregunta6}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta6"
//                         Value={form.Pregunta6}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Determina oportunidades y amenazas a través
//                      del análisis integral de las tendencias actuales y futuras. comprende cuál es el escenario más amplio.</Label>

//                     <hr></hr>




//                     <Label>7- Pragmatismo:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta7"
//                         Value={form.Pregunta7}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta7"
//                         Value={form.Pregunta7}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta7"
//                         Value={form.Pregunta7}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta7"
//                         Value={form.Pregunta7}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta7"
//                         Value={form.Pregunta7}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Genera soluciones sensatas,realistas y prácticas para los problemas.</Label>

//                     <hr></hr>




//                     <Label>8- Aceptación De Riesgos:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta8"
//                         Value={form.Pregunta8}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta8"
//                         Value={form.Pregunta8}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta8"
//                         Value={form.Pregunta8}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta8"
//                         Value={form.Pregunta8}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta8"
//                         Value={form.Pregunta8}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Toma riesgos calculados, que generalmente resultan bien. no apuesta lo que no puede perder.</Label>

//                     <hr></hr>




//                     <Label>9- Vanguardismo:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta9"
//                         Value={form.Pregunta9}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta9"
//                         Value={form.Pregunta9}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta9"
//                         Value={form.Pregunta9}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta9"
//                         Value={form.Pregunta9}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta9"
//                         Value={form.Pregunta9}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Siempre se compara con las mejores
//                     prácticas y espera que los demás hagan lo mismo.</Label>

//                     <hr></hr>



//                     <Label>10- Educación:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta10"
//                         Value={form.Pregunta10}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta10"
//                         Value={form.Pregunta10}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta10"
//                         Value={form.Pregunta10}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta10"
//                         Value={form.Pregunta10}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta10"
//                         Value={form.Pregunta10}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Cumple con los requisitos de educación en
//                     ambientes formales e informales. aprende continuamente</Label>

//                     <hr></hr>




//                     <Label>11- Experiencia:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta11"
//                         Value={form.Pregunta11}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta11"
//                         Value={form.Pregunta11}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta11"
//                         Value={form.Pregunta11}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta11"
//                         Value={form.Pregunta11}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta11"
//                         Value={form.Pregunta11}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Específica para cada puesto.</Label>

//                     <hr></hr>




//                     <Label>12- Antecedentes:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta12"
//                         Value={form.Pregunta12}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta12"
//                         Value={form.Pregunta12}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta12"
//                         Value={form.Pregunta12}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta12"
//                         Value={form.Pregunta12}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta12"
//                         Value={form.Pregunta12}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Historial profesional de éxito. generalmente
//                     su desempeño es calificado como “Excelente”.</Label>

//                     <hr></hr>




//                     <Label>13- Integridad:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta13"
//                         Value={form.Pregunta13}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta13"
//                         Value={form.Pregunta13}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta13"
//                         Value={form.Pregunta13}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta13"
//                         Value={form.Pregunta13}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta13"
//                         Value={form.Pregunta13}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Incuestionable. no toma atajos poco éticos.se gana la
//                     confianza de sus colaboradores. pone los intereses de la organización por encima de los suyos.</Label>

//                     <hr></hr>




//                     <Label>14- Inventiva/ Iniciativa:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta14"
//                         Value={form.Pregunta14}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta14"
//                         Value={form.Pregunta14}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta14"
//                         Value={form.Pregunta14}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta14"
//                         Value={form.Pregunta14}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta14"
//                         Value={form.Pregunta14}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Busca apasionadamente la solución de los problemas.
//                     logra resultados aunque falten recursos. hace más de lo que se le pide.
//                      tiende a la acción y busca resultados.</Label>

//                     <hr></hr>




//                     <Label>15- Organización o Planeación:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta15"
//                         Value={form.Pregunta15}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta15"
//                         Value={form.Pregunta15}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta15"
//                         Value={form.Pregunta15}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta15"
//                         Value={form.Pregunta15}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta15"
//                         Value={form.Pregunta15}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Planea, organiza, programa y presupuesta en forma eficiente y organizada. se centra en las prioridades.</Label>

//                     <hr></hr>




//                     <Label>16- Excelencia:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta16"
//                         Value={form.Pregunta16}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta16"
//                         Value={form.Pregunta16}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta16"
//                         Value={form.Pregunta16}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta16"
//                         Value={form.Pregunta16}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta16"
//                         Value={form.Pregunta16}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Establece estándares de desempeño altos para sí
//                     mismo y para los demás. tiene poca tolerancia para la
//                     mediocridad. alto sentido de responsabilidad.</Label>

//                     <hr></hr>




//                     <Label>17- Independencia:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta17"
//                         Value={form.Pregunta17}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta17"
//                         Value={form.Pregunta17}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta17"
//                         Value={form.Pregunta17}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta17"
//                         Value={form.Pregunta17}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta17"
//                         Value={form.Pregunta17}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Desea ser independiente al tomar posturas. no cambia de opinión cada vez que habla con alguien.</Label>

//                     <hr></hr>




//                     <Label>18- Manejo Del Estrés:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta18"
//                         Value={form.Pregunta}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta18"
//                         Value={form.Pregunta18}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta18"
//                         Value={form.Pregunta18}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta18"
//                         Value={form.Pregunta18}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta18"
//                         Value={form.Pregunta18}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Mantiene la estabilidad y la compostura también bajo presión.</Label>

//                     <hr></hr>




//                     <Label>19- Consciencia De Sí Mismo/ Retroalimentación:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta19"
//                         Value={form.Pregunta19}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta19"
//                         Value={form.Pregunta19}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta19"
//                         Value={form.Pregunta19}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta19"
//                         Value={form.Pregunta19}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta19"
//                         Value={form.Pregunta19}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Reconoce sus fortalezas y debilidades. no está a la defensiva.
//                     no racionaliza los errores ni culpa a otros.
//                     usa mecanismos de retroalimentación.</Label>

//                     <hr></hr>




//                     <Label>20- Adaptabilidad:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta20"
//                         Value={form.Pregunta20}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta20"
//                         Value={form.Pregunta20}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta20"
//                         Value={form.Pregunta20}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta20"
//                         Value={form.Pregunta20}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta20"
//                         Value={form.Pregunta20}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>No es rígido. lidia eficazmente con las dificultades y cambios.</Label>

//                     <hr></hr>




//                     <Label>21- Primera Impresión:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta21"
//                         Value={form.Pregunta21}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta21"
//                         Value={form.Pregunta21}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta21"
//                         Value={form.Pregunta21}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta21"
//                         Value={form.Pregunta21}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta21"
//                         Value={form.Pregunta21}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Muestra profesionalismo. produce una primera impresión favorable,
//                     lenguaje corporal, contacto visual,
//                      postura, etc.</Label>

//                     <hr></hr>




//                     <Label>22- Simpatía:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta22"
//                         Value={form.Pregunta22}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta22"
//                         Value={form.Pregunta22}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta22"
//                         Value={form.Pregunta22}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta22"
//                         Value={form.Pregunta22}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta22"
//                         Value={form.Pregunta22}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Hace que los demás se sientan cómodos. muestra inteligencia emocional.
//                     es cálido, sensible, compasivo. no es arrogante.
//                     es amigable, tiene sentido del humor, es auténtico.</Label>

//                     <hr></hr>




//                     <Label>23- Saber Escuchar:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta23"
//                         Value={form.Pregunta23}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta23"
//                         Value={form.Pregunta23}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta23"
//                         Value={form.Pregunta23}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta23"
//                         Value={form.Pregunta23}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta23"
//                         Value={form.Pregunta23}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Se sintoniza con las opiniones,
//                     los sentimientos y las necesidades de los demás.
//                     es empático y paciente. deja hablar a los demás y
//                     los escucha activamente.</Label>

//                     <hr></hr>




//                     <Label>24- Enfoque En Los Clientes:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta24"
//                         Value={form.Pregunta24}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta24"
//                         Value={form.Pregunta24}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta24"
//                         Value={form.Pregunta24}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta24"
//                         Value={form.Pregunta24}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta24"
//                         Value={form.Pregunta24}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Monitorea la satisfacción de los clientes (Internos O Externos).
//                     establece relaciones de socio con los clientes.
//                      está visible y accesible para los cliente.</Label>

//                     <hr></hr>




//                     <Label>25- Espíritu De Equipo:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta25"
//                         Value={form.Pregunta25}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta25"
//                         Value={form.Pregunta25}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta25"
//                         Value={form.Pregunta25}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta25"
//                         Value={form.Pregunta25}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta25"
//                         Value={form.Pregunta25}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Ayuda a sus compañeros. hace equipo con ellos.
//                     es accesible.
//                     logra que los demás hagan lo que le conviene a la empresa.</Label>

//                     <hr></hr>




//                     <Label>26- Asertividad:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta26"
//                         Value={form.Pregunta26}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta26"
//                         Value={form.Pregunta26}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta26"
//                         Value={form.Pregunta26}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta26"
//                         Value={form.Pregunta26}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta26"
//                         Value={form.Pregunta26}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Toma posturas sin ser demasiado abrasivo.</Label>

//                     <hr></hr>




//                     <Label>27- Comunicación "Oral":</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta27"
//                         Value={form.Pregunta27}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta27"
//                         Value={form.Pregunta27}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta27"
//                         Value={form.Pregunta27}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta27"
//                         Value={form.Pregunta27}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta27"
//                         Value={form.Pregunta27}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Se comunica bien cara a cara,en grupos pequeños y en público.
//                     habla con fluidez,
//                     domina el lenguaje y es elocuente.
//                     mantiene a los demás informados.</Label>

//                     <hr></hr>




//                     <Label>28- Comunicación "Escrita":</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta28"
//                         Value={form.Pregunta28}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta28"
//                         Value={form.Pregunta28}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta28"
//                         Value={form.Pregunta28}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta28"
//                         Value={form.Pregunta28}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta28"
//                         Value={form.Pregunta28}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Redacta documentos claros, precisos, bien organizados,
//                     usando un lenguaje adecuado y buena gramática y sintaxis.</Label>

//                     <hr></hr>




//                     <Label>29- Sabiduría Política:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta29"
//                         Value={form.Pregunta29}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta29"
//                         Value={form.Pregunta29}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta29"
//                         Value={form.Pregunta29}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta29"
//                         Value={form.Pregunta29}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta29"
//                         Value={form.Pregunta29}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Está consciente de los intereses ocultos y aplica con eficacia esta consciencia.</Label>

//                     <hr></hr>




//                     <Label>30- Habilidad Para Negociar:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta30"
//                         Value={form.Pregunta}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta30"
//                         Value={form.Pregunta30}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta30"
//                         Value={form.Pregunta30}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta30"
//                         Value={form.Pregunta30}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta30"
//                         Value={form.Pregunta30}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Logra buenos resultados en las negociaciones para que todos salgan ganando.</Label>

//                     <hr></hr>




//                     <Label>31- Persuasivo:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta31"
//                         Value={form.Pregunta31}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta31"
//                         Value={form.Pregunta31}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta31"
//                         Value={form.Pregunta31}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta31"
//                         Value={form.Pregunta31}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta31"
//                         Value={form.Pregunta31}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Es persuasivo y convincente para vender su visión.</Label>

//                     <hr></hr>




//                     <Label>32- Capacitación/ Desarrollo/ Coaching:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta32"
//                         Value={form.Pregunta32}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta32"
//                         Value={form.Pregunta32}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta32"
//                         Value={form.Pregunta32}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta32"
//                         Value={form.Pregunta32}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta32"
//                         Value={form.Pregunta32}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Capacita activamente y con éxito a las personas.
//                     ofrece coaching y desarrollo para que la gente
//                     sea promovida cuando lo merece.</Label>

//                     <hr></hr>




//                     <Label>33- Establecer Metas:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta33"
//                         Value={form.Pregunta33}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta33"
//                         Value={form.Pregunta33}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta33"
//                         Value={form.Pregunta33}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta33"
//                         Value={form.Pregunta33}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta33"
//                         Value={form.Pregunta33}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Establece metas factibles para sí mismo y para los demás. alienta la iniciativa personal.</Label>

//                     <hr></hr>




//                     <Label>34- Empoderar A Otros:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta34"
//                         Value={form.Pregunta34}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta34"
//                         Value={form.Pregunta34}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta34"
//                         Value={form.Pregunta34}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta34"
//                         Value={form.Pregunta34}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta34"
//                         Value={form.Pregunta34}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Empuja a sus subalternos a tomar decisiones. les da autoridad y recursos.</Label>

//                     <hr></hr>




//                     <Label>35- Dirección Del Desempeño:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta35"
//                         Value={form.Pregunta35}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta35"
//                         Value={form.Pregunta35}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta35"
//                         Value={form.Pregunta35}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta35"
//                         Value={form.Pregunta35}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta35"
//                         Value={form.Pregunta35}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Promueve un alto nivel de responsabilidad a través de sistemas de dirección justos y alentadores.
//                     elogia y reconoce a quienes lo merecen.
//                     hace críticas constructivas.
//                     brinda retroalimentación a su gente.</Label>

//                     <hr></hr>




//                     <Label>36- Creación De Equipo:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta36"
//                         Value={form.Pregunta36}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta36"
//                         Value={form.Pregunta36}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta36"
//                         Value={form.Pregunta36}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta36"
//                         Value={form.Pregunta36}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta36"
//                         Value={form.Pregunta36}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Logra crear un espíritu de cohesión y
//                     de equipo eficazmente. trata a las personas con justicia.
//                     comparte el crédito de los logros.</Label>

//                     <hr></hr>




//                     <Label>Conducción De Reuniones:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta37"
//                         Value={form.Pregunta37}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta37"
//                         Value={form.Pregunta37}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta37"
//                         Value={form.Pregunta37}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta37"
//                         Value={form.Pregunta37}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta37"
//                         Value={form.Pregunta37}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Demuestra capacidad para organizar y conducir reuniones eficaces.</Label>

//                     <hr></hr>




//                     <Label>38- Visión:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta38"
//                         Value={form.Pregunta38}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta38"
//                         Value={form.Pregunta38}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta38"
//                         Value={form.Pregunta38}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta38"
//                         Value={form.Pregunta38}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta38"
//                         Value={form.Pregunta38}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Tiene una visión clara y creíble, y una estrategia.</Label>

//                     <hr></hr>




//                     <Label>39- Liderazgo En Cambios:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta39"
//                         Value={form.Pregunta39}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta39"
//                         Value={form.Pregunta39}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta39"
//                         Value={form.Pregunta39}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta39"
//                         Value={form.Pregunta39}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta39"
//                         Value={form.Pregunta39}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Interviene activamente para crear cambios positivos. dirige con el ejemplo.</Label>

//                     <hr></hr>




//                     <Label>40- Inspira Y Es Seguido:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta40"
//                         Value={form.Pregunta40}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta40"
//                         Value={form.Pregunta40}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta40"
//                         Value={form.Pregunta40}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta40"
//                         Value={form.Pregunta40}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta40"
//                         Value={form.Pregunta40}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>inspira a la gente a seguirlo. no es muy intimidante. se hace cargo. sabe motivar a los demás.</Label>

//                     <hr></hr>




//                     <Label>41- Manejo De Conflictos:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta41"
//                         Value={form.Pregunta41}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta41"
//                         Value={form.Pregunta41}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta41"
//                         Value={form.Pregunta41}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta41"
//                         Value={form.Pregunta41}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta41"
//                         Value={form.Pregunta41}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Entiende las fuerzas naturales de los conflictos y actúa para evitarlos o limarlos.
//                     trabaja con eficacia durante los conflictos para optimizar los resultados.
//                     no reprime, ignora o niega los conflictos.</Label>

//                     <hr></hr>




//                     <Label>42- Energía/ Motivación:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta42"
//                         Value={form.Pregunta42}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta42"
//                         Value={form.Pregunta42}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta42"
//                         Value={form.Pregunta42}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta42"
//                         Value={form.Pregunta42}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta42"
//                         Value={form.Pregunta42}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Muestra energía, un fuerte deseo de lograr las metas, un alto nivel de dedicación.
//                     trabaja 60 horas o más por semana para lograr los resultados que espera.</Label>

//                     <hr></hr>




//                     <Label>43- Entusiasmo O Pasión:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta43"
//                         Value={form.Pregunta43}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta43"
//                         Value={form.Pregunta43}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta43"
//                         Value={form.Pregunta43}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta43"
//                         Value={form.Pregunta43}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta43"
//                         Value={form.Pregunta43}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Muestra dinamismo, emoción y una actitud positiva de poder lograr sus metas.</Label>

//                     <hr></hr>




//                     <Label>44- Ambición:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta44"
//                         Value={form.Pregunta44}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta44"
//                         Value={form.Pregunta44}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta44"
//                         Value={form.Pregunta44}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta44"
//                         Value={form.Pregunta44}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta44"
//                         Value={form.Pregunta44}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Quiere tener más responsabilidades y autoridad.</Label>

//                     <hr></hr>




//                     <Label>45- Compatibilidad De Necesidades:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta45"
//                         Value={form.Pregunta45}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta45"
//                         Value={form.Pregunta45}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta45"
//                         Value={form.Pregunta45}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta45"
//                         Value={form.Pregunta45}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta45"
//                         Value={form.Pregunta45}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Sus necesidades corresponden A las oportunidades del futuro previsible.</Label>

//                     <hr></hr>




//                     <Label>46- Equilibrio En La Vida:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta46"
//                         Value={form.Pregunta46}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta46"
//                         Value={form.Pregunta46}

//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta46"
//                         Value={form.Pregunta46}

//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta46"
//                         Value={form.Pregunta46}

//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta46"
//                         Value={form.Pregunta46}

//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label >Mal</label>
//                     <Label>Logra suficiente balance entre el trabajo, el bienestar,
//                     las relaciones, la participación en la comunidad, los contactos profesionales,
//                     las amistades, los pasatiempos y sus intereses.</Label>

//                     <hr></hr>




//                     <Label>47- Tenacidad:</Label>
//                     <InputRadio
//                         type="radio"
//                         id="5"
//                         name="Pregunta47"
//                         Value={form.Pregunta47}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Excelente</label>
//                     <InputRadio
//                         type="radio"
//                         id="4"
//                         name="Pregunta47"
//                         Value={form.Pregunta47}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Muy Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="3"
//                         name="Pregunta47"
//                         Value={form.Pregunta47}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Bien</label>
//                     <InputRadio
//                         type="radio"
//                         id="2"
//                         name="Pregunta47"
//                         Value={form.Pregunta47}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Suficiente</label>
//                     <InputRadio
//                         type="radio"
//                         id="1"
//                         name="Pregunta47"
//                         Value={form.Pregunta47}
//                         onBlur={handleBlur}
//                         onChange={handleChange}
//                         required
//                     />
//                     <label>Mal</label>
//                     <Label>Le satisface buscar apasionadamente el logro de los resultados.
//                     proyecta sus ganas de ganar. tiene fama de no darse por vencido.</Label>
//                     <hr></hr> */}


// {/* 

//                 </div>
//                 <ContenedorBotonCentrado>
//                     <Link className="Anterior" to="/pagina3">Anterior</Link>
//                     <Boton type="submit" value="Enviar">Enviar</Boton>
//                 </ContenedorBotonCentrado>

//             </FormularioPreguntas>


//         </>
//     );
// };

// export default Preguntas; */}
