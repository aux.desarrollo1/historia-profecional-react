import { useForm } from "../componentes/validaciondos";
import React from "react";
import {
    Formulario,
    Link,
    Label,
    LabelRadio,
    Input,
    InputTrabajo,
    ContenedorBotonCentrado,
    Boton,
    Div,
    // FormularioPreguntas,
    Formulario1,
    InputRadio,
} from "../elementos/Formularios2";

const initialForm = {
    Trabajos: "",
    Ciudad: "",
    Empresa: "",
    Direccion: "",
    Estado: "",
    Codigo: "",
    Telefono: "",
    Giro: "",
    FechaIni: "",
    FechaFin: "",
    Puesto: "",
    Pregunta1: "",
    Personas: "",
    Sueldo: "",
    RemuneracionIni: "",
    RemuneracionFin: "",
    Bono: "",
    Ingreso: "",
    Supervisor: "",
    Puesto_2: "",
    Pregunta2: "",
    Pregunta3: "",
    ExisteCircustancia: "",
    Razon: "",
    MenosGustaba: "",
    MasGustaba: ""
};

const validationsForm = (Formulario) => {
    let Errors = {};
    let Nombre = /^[a-zA-ZÀ-ÿ\s]{1,40}$/; // Letras y espacios, pueden llevar acentos.
    let Postal = /^\d{5,5}$/; // 7 a 14
    let Personas = /^\d{1,5}$/; // 7 a 14
    let Telefono = /^\d{7,10}$/; // 7 a 14
    let Generaal = /^.{1,20}$/;
    let Datos = /^.{1,10}$/;
    let letras = /^[A-Z]+$/i;
    let Lugar = /^[a-zA-Z0-9 ]+$/;
    let Fecha = /^[0-9]+$/;

    if (!Formulario.Empresa.trim()) { } else if (!Datos.test(Formulario.Empresa.trim())) {
        Errors.Empresa = "Solo acepta maximo 10";
    }
    if (!Formulario.Direccion.trim()) { } else if (!Lugar.test(Formulario.Direccion.trim())) {
        Errors.Direccion = "Sólo acepta letras y numeros.";
    }
    if (!Formulario.Ciudad.trim()) { } else if (!Nombre.test(Formulario.Ciudad.trim())) {
        Errors.Ciudad = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.Estado.trim()) { } else if (!Nombre.test(Formulario.Estado.trim())) {
        Errors.Estado = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.Codigo.trim()) { } else if (!Postal.test(Formulario.Codigo.trim())) {
        Errors.Codigo = "Sólo acepta 5 Numeros";
    }
    if (!Formulario.Telefono.trim()) { } else if (!Telefono.test(Formulario.Telefono.trim())) {
        Errors.Telefono = "Sólo acepta 7 a 10 numeros";
    }
    if (!Formulario.Giro.trim()) { } else if (!letras.test(Formulario.Giro.trim())) {
        Errors.Giro = "Solo acepta letras";
    }
    if (!Formulario.Puesto.trim()) { } else if (!letras.test(Formulario.Puesto.trim())) {
        Errors.Puesto = "Sólo acepta letras";
    }
    if (!Formulario.Personas.trim()) { } else if (!Personas.test(Formulario.Personas.trim())) {
        Errors.Personas = "Sólo acepta Numeros y maximo 5 Numeros";
    }
    if (!Formulario.Sueldo.trim()) { } else if (!Fecha.test(Formulario.Sueldo.trim())) {
        Errors.Sueldo = "Sólo acepta numeros";
    }
    if (!Formulario.RemuneracionIni.trim()) { } else if (!Fecha.test(Formulario.RemuneracionIni.trim())) {
        Errors.RemuneracionIni = "Sólo acepta numeros";
    }
    if (!Formulario.RemuneracionFin.trim()) { } else if (!Fecha.test(Formulario.RemuneracionFin.trim())) {
        Errors.RemuneracionFin = "Sólo acepta numeros";
    }
    if (!Formulario.RemuneracionFin.trim()) { } else if (!Fecha.test(Formulario.RemuneracionFin.trim())) {
        Errors.RemuneracionFin = "Sólo acepta numeros";
    }
    if (!Formulario.Bono.trim()) { } else if (!Fecha.test(Formulario.Bono.trim())) {
        Errors.Bono = "Sólo acepta numeros";
    }
    if (!Formulario.Ingreso.trim()) { } else if (!Fecha.test(Formulario.Ingreso.trim())) {
        Errors.Ingreso = "Sólo acepta numeros";
    }
    if (!Formulario.Supervisor.trim()) { } else if (!Nombre.test(Formulario.Supervisor.trim())) {
        Errors.Supervisor = "Sólo acepta letras";
    }
    if (!Formulario.Puesto_2.trim()) { } else if (!Generaal.test(Formulario.Puesto_2.trim())) {
        Errors.Puesto_2 = "Solo acepta maximo 20 caracteres";
    }
    if (!Formulario.MasGustaba.trim()) { } else if (!letras.test(Formulario.MasGustaba.trim())) {
        Errors.MasGustaba = "Solo acepta letras";
    }
    if (!Formulario.MenosGustaba.trim()) { } else if (!letras.test(Formulario.MenosGustaba.trim())) {
        Errors.MenosGustaba = "Solo acepta letras";
    }
    if (!Formulario.ExisteCircustancia.trim()) { } else if (!letras.test(Formulario.ExisteCircustancia.trim())) {
        Errors.ExisteCircustancia = "Solo acepta letras";
    }
    if (!Formulario.Razon.trim()) { } else if (!letras.test(Formulario.Razon.trim())) {
        Errors.Razon = "Solo acepta letras";
    }
    return Errors;


};

let styles = {
    fontWeight: "bold",
    color: "#dc3545",
};




// const Nav=(props)=>{
//     const{history}=props;
// };



const ExperenciaLaboral = () => {
    const {
        form,
        errors,
        handleChange,
        handleBlur,
        handleSubmit,
    } = useForm(initialForm, validationsForm);
    return (
        <>
            <Formulario1>

                <h2>Experencia Laboral</h2>
                <Label htmlFor="Trabajos">Agregar Primer Trabajo Reciente</Label>
                <InputTrabajo type="checkbox" id="Trabajo1" name="Trabajo" />
                <p></p>
                <Label htmlFor="Trabajos">Agregar Segundo Trabajo Reciente</Label>
                <InputTrabajo type="checkbox" id="Trabajo2" name="Trabajo" />
                <p></p>
                <Label htmlFor="Trabajos">Agregar Tercer Trabajo Reciente</Label>
                <InputTrabajo type="checkbox" id="Trabajo2" name="Trabajo" />



            </Formulario1>

            <Formulario onSubmit={handleSubmit}>
                <Label htmlFor="Empresa">Empresa:
                <Input type="text"
                        id="Empresa"
                        name="Empresa"
                        placeholder="Empresa"
                        Value={form.Empresa}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Empresa && <p style={styles}>{errors.Empresa}</p>}
                </Label>
                <Label htmlFor="Direccion">Dirección:
                <Input type="text"
                        id="Direccion"
                        name="Direccion"
                        Value={form.Direccion}
                        placeholder="Dirección"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Direccion && <p style={styles}>{errors.Direccion}</p>}
                </Label>
                <Label htmlFor="Ciudad">Ciudad:
                <Input type="text"
                        id="Ciudad"
                        name="Ciudad"
                        Value={form.Ciudad}
                        placeholder="Ciudad"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Ciudad && <p style={styles}>{errors.Ciudad}</p>}
                </Label>
                <Label htmlFor="Estado">Estado:
                <Input type="text"
                        id="Estado"
                        name="Estado"
                        Value={form.Estado}
                        placeholder="Estado"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Estado && <p style={styles}>{errors.Estado}</p>}
                </Label>
                <Label htmlFor="Codigo">Código Postal:
                <Input type="text"
                        id="Codigo"
                        name="Codigo"
                        Value={form.Codigo}
                        placeholder="Código Postal"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Codigo && <p style={styles}>{errors.Codigo}</p>}
                </Label>
                <Label htmlFor="Telefono">Número Telefónico O Celular:
                <Input type="num"
                        id="Telefono"
                        name="Telefono"
                        Value={form.Telefono}
                        placeholder="Número Telefónico O Celular"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Telefono && <p style={styles}>{errors.Telefono}</p>}
                </Label>
                <Label htmlFor="Giro">Giro Del Negocio:
                <Input type="text"
                        id="Giro"
                        name="Giro"
                        Value={form.Giro}
                        placeholder="Giro Del Negocio"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Giro && <p style={styles}>{errors.Giro}</p>}
                </Label>
                <Label htmlFor="FechaIni">Fecha De Inicio A Laborar:
                <Input type="date"
                        id="FechaIni"
                        name="FechaIni"
                        Value={form.FechaIni}
                        placeholder="Fecha De Inicio A Laborar"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                </Label>
                <Label htmlFor="FechaFin">Fecha Final De Labores:
                <Input type="date"
                        id="FechaFin"
                        name="FechaFin"
                        Value={form.FechaFin}
                        placeholder="Fecha Final De Labores"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                </Label>
                <Label htmlFor="Puesto">Puesto Que Desempeñaba:
                <Input type="text"
                        id="Puesto"
                        name="Puesto"
                        Value={form.Puesto}
                        placeholder="Puesto Que Desempeñaba"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Puesto && <p style={styles}>{errors.Puesto}</p>}
                </Label>
                {/* <Radio> */}
                    <Label>¿Contaba Con Personal A Su Cargo?
                        <InputRadio
                            type="radio"
                            id="1"
                            name="Pregunta1"
                            Value={form.Pregunta1}
                            // value="Si"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            required
                        />
                        <LabelRadio htmlFor="Si">Si</LabelRadio>
                        <InputRadio
                            type="radio"
                            id="0"
                            name="Pregunta1"
                            Value={form.Pregunta1}
                            // value="No"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            required
                        />
                        <LabelRadio htmlFor="0">No</LabelRadio>
                    </Label>
                {/* </Radio> */}
                <div id="Ocultar">
                <Label htmlFor="Personas">Cuántas Personas:
                <Input type="text"
                        id="Personas"
                        name="Personas"
                        placeholder="Numero de Personas"
                        Value={form.Personas}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Personas && <p style={styles}>{errors.Personas}</p>}
                </Label>
                </div>




                <Label htmlFor="Sueldo">Sueldo Base:
                <Input type="text"
                        id="Sueldo"
                        name="Sueldo"
                        Value={form.Sueldo}
                        placeholder="Sueldo Base"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Sueldo && <p style={styles}>{errors.Sueldo}</p>}
                </Label>
                <Label htmlFor="RemuneracionIni">Remuneración Total (Inicial):
                <Input type="text"
                        id="RemuneracionIni"
                        name="RemuneracionIni"
                        Value={form.RemuneracionIni}
                        placeholder="Remuneración Total Inicial"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.RemuneracionIni && <p style={styles}>{errors.RemuneracionIni}</p>}
                </Label>
                <Label htmlFor="RemuneracionFin">Remuneración Total (Final):
                <Input type="text"
                        id="RemuneracionFin"
                        name="RemuneracionFin"
                        Value={form.RemuneracionFin}
                        placeholder="Remuneración Total Final"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.RemuneracionFin && <p style={styles}>{errors.RemuneracionFin}</p>}
                </Label>
                <Label htmlFor="Bono">Bono:
                <Input type="text"
                        id="Bono"
                        name="Bono"
                        Value={form.Bono}
                        placeholder="Bono"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Bono && <p style={styles}>{errors.Bono}</p>}
                </Label>
                <Label htmlFor="OIngreso">Otro Ingreso:
                <Input type="text"
                        id="Ingreso"
                        name="Ingreso"
                        Value={form.Ingreso}
                        placeholder="Otro Ingreso"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Ingreso && <p style={styles}>{errors.Ingreso}</p>}
                </Label>
                <Label htmlFor="Supervisor">Nombre Del Supervisor Inmediato:
                <Input type="text"
                        id="Supervisor"
                        name="Supervisor"
                        Value={form.Supervisor}
                        placeholder="Nombre Del Supervisor Inmediato"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Supervisor && <p style={styles}>{errors.Supervisor}</p>}
                </Label>
                <Label htmlFor="Puesto_2">Puesto:
                <Input type="text"
                        id="Puesto_2"
                        name="Puesto_2"
                        Value={form.Puesto_2}
                        placeholder="Puesto"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Puesto_2 && <p style={styles}>{errors.Puesto_2}</p>}
                </Label>
                {/* <Radio> */}
                    <Div>
                    <Label>¿Qué cálificacion estima que le daria este supervisor respecto a su desempeño general?</Label>
                    <InputRadio
                        type="radio"
                        id="Excelente"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Excelente"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Excelente">Excelente</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Muy_bien"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Muy_bien"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Muy_bien">Muy bien</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Bien"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Bien"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Bien">Bien</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Satisfactorio"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Satisfactorio"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Satisfactorio">Satisfactorio</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Malo"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Malo"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Malo">Malo</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Imposible_De_Facilitar"
                        name="Pregunta2"
                        Value={form.Pregunta2}

                        // value="Imposible_De_Facilitar"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="Imposible_De_Facilitar">Imposible De Facilitar</LabelRadio>
                </Div>
                {/* </Radio> */}
                <Label htmlFor="MasGustaba">¿Qué es lo que más le gusta (gustaba) de su trabajo?
                <Input type="text"
                        id="MasGustaba"
                        name="MasGustaba"
                        Value={form.MasGustaba}
                        placeholder="Lo Que Mas Le Gustaba De Su Trabajo"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.MasGustaba && <p style={styles}>{errors.MasGustaba}</p>}
                </Label>
                <Label htmlFor="MenosGustaba">¿Qué Es Lo Que Menos Le Gusta (Gustaba) De Su Trabajo?
                <Input type="text"
                        id="MenosGustaba"
                        name="MenosGustaba"
                        Value={form.MenosGustaba}
                        placeholder="Lo Que Menos Le Gustaba De Su Trabajo"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.MenosGustaba && <p style={styles}>{errors.MenosGustaba}</p>}
                </Label>


                {/* <Radio> */}
                    <Div>

                    <Label>¿Si se retiró de la empresa, por favor indique su motivo para hacerlo?</Label>
                    <InputRadio
                        type="radio"
                        id="Mi_Decisíon"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    // value="Mi_Decisíon"
                    />
                    <LabelRadio htmlFor="Mi_Decisíon">100% Mi Decisíon</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Decision_Mutua"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        // value="Decision_Mutua"
                        required
                    />
                    <LabelRadio htmlFor="Decision_Mutua">Decision Mutua</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Decision_Empresa"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        // value="Decision_Empresa"
                        required
                    />
                    <LabelRadio htmlFor="Decision_Empresa">Me Despidieron</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="Otra_Circunstancias"
                        name="Pregunta3"
                        Value={form.Pregunta3}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        // value="Otra_Circunstancias"
                        required
                    />
                    <LabelRadio htmlFor="Otra_Circunstancias">Otra Circunstancias</LabelRadio>
                    </Div>
                
                {/* </Radio> */}
                <Label htmlFor="ExisteCircustancia">Existe Otra Circustancia,Por Favor Expliquela:
                <Input type="text"
                        id="ExisteCircustancia"
                        name="ExisteCircustancia"
                        Value={form.ExisteCircustancia}
                        placeholder="Circustancia,Por Favor Expliquela"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.ExisteCircustancia && <p style={styles}>{errors.ExisteCircustancia}</p>}
                </Label>
                <Label htmlFor="Razon">Razón(es) Para Retirarse De la Empresa:
                <Input type="text"
                        id="Razon"
                        name="Razon"
                        Value={form.Razon}
                        placeholder="Razones Para Retirarse De la Empresa"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Razon && <p style={styles}>{errors.Razon}</p>}
                </Label>
                {/* <FormularioPreguntas> */}

                <ContenedorBotonCentrado>
                    <Link className="Anterior" to="/pagina1">Anterior</Link>
                    <Boton type="submit" value="Enviar">Siguiente</Boton>
                </ContenedorBotonCentrado>
                {/* </FormularioPreguntas> */}

            </Formulario>
        </>
    );

};

export default ExperenciaLaboral;