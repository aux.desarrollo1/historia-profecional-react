import React from "react";
import { useForm } from "../componentes/validaciontres";
import {
    Formulario,
    Label,
    Div,
    Link,
    LabelRadio,
    Input,
    ContenedorBotonCentrado,
    Boton,
    Formulario1,
    InputRadio,
} from "../elementos/Formularios2";

const initialForm = {
    Pregunta1: "",
    NombreUbicacion: "",
    FechaIni: "",
    FechaFin: "",
    Grados: "",
    Promedio: "",
    CreditoEscolares: "",
    Actividades: "",
    Trabajo: "",
    Pregunta2: "",
    FechaIniUni: "",
    FechaFinUni: "",
    GradosUni: "",
    PromedioUni: "",
    CreditoEscolaresUni: "",
    ActividadesUni: "",
    Pregunta1Uni: "",
    Pregunta2Uni: "",
    Pregunta3Uni: "",
    Pregunta4Uni: "",
    Pregunta5Uni: "",
    Pregunta6Uni: "",
    NombreUbicacionUni: "",
};


const validationsForm = (Formulario) => {
    let Errors = {};
    let Nombre = /^[a-zA-ZÀ-ÿ\s]{1,40}$/; // Letras y espacios, pueden llevar acentos.
    let Generaal = /^.{1,20}$/;
    let letras = /^[A-Z]+$/i;
    let Lugar = /^[a-zA-Z0-9 ]+$/;
    let Promedio2 = /^[0-9]+([.])?([0-9]+)?$/;

    if (!Formulario.NombreUbicacion.trim()) { } else if (!Nombre.test(Formulario.NombreUbicacion.trim())) {
        Errors.NombreUbicacion = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.Grados.trim()) { } else if (!Generaal.test(Formulario.Grados.trim())) {
        Errors.Grados = "limite de caracteres aceptados";
    }
    if (!Formulario.Promedio.trim()) { } else if (!Promedio2.test(Formulario.Promedio.trim())) {
        Errors.Promedio = "Sólo acepta del 0 al 100  de calificacion";
    }
    if (!Formulario.CreditoEscolares.trim()) { } else if (!letras.test(Formulario.CreditoEscolares.trim())) {
        Errors.CreditoEscolares = "Sólo acepta letras";
    }
    if (!Formulario.Actividades.trim()) { } else if (!letras.test(Formulario.Actividades.trim())) {
        Errors.Actividades = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.Trabajo.trim()) { } else if (!Nombre.test(Formulario.Trabajo.trim())) {
        Errors.Trabajo = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.NombreUbicacionUni.trim()) { } else if (!Nombre.test(Formulario.NombreUbicacionUni.trim())) {
        Errors.NombreUbicacionUni = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.GradosUni.trim()) { } else if (!Generaal.test(Formulario.GradosUni.trim())) {
        Errors.GradosUni = "limite de caracteres aceptados";
    }
    if (!Formulario.PromedioUni.trim()) { } else if (!Promedio2.test(Formulario.PromedioUni.trim())) {
        Errors.PromedioUni = "Sólo acepta del 0 al 100  de calificacion";
    }
    if (!Formulario.CreditoEscolaresUni.trim()) { } else if (!letras.test(Formulario.CreditoEscolaresUni.trim())) {
        Errors.CreditoEscolaresUni = "Sólo acepta letras";
    }
    if (!Formulario.ActividadesUni.trim()) { } else if (!letras.test(Formulario.ActividadesUni.trim())) {
        Errors.ActividadesUni = "Sólo acepta letras y espacios en blanco";
    }
    if (!Formulario.Pregunta1Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta1Uni.trim())) {
        Errors.Pregunta1Uni = "Sólo acepta letras y numeros";
    }
    if (!Formulario.Pregunta2Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta2Uni.trim())) {
        Errors.Pregunta2Uni = "Sólo acepta letras y numeros";
    }
    if (!Formulario.Pregunta3Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta3Uni.trim())) {
        Errors.Pregunta3Uni = "Sólo acepta letras y numeros";
    }
    if (!Formulario.Pregunta4Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta4Uni.trim())) {
        Errors.Pregunta4Uni = "Sólo acepta letras y numeros";
    }
    if (!Formulario.Pregunta5Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta5Uni.trim())) {
        Errors.Pregunta5Uni = "Sólo acepta letras y numeros";
    }
    if (!Formulario.Pregunta6Uni.trim()) { } else if (!Lugar.test(Formulario.Pregunta6Uni.trim())) {
        Errors.Pregunta6Uni = "Sólo acepta letras y numeros";
    }
    return Errors;
};

let styles = {
    fontWeight: "bold",
    color: "#dc3545",
};

const Educacion = () => {
    const {
        form,
        errors,
        // loading,
        // response,
        handleChange,
        handleBlur,
        handleSubmit,
    } = useForm(initialForm, validationsForm);
    return (
        <>
            <Formulario1>
                <h3>Educación</h3>
            </Formulario1>
            <Formulario onSubmit={handleSubmit}>


                {/* <Radio> */}
                <Div>

                    <Label>Preparatoria (AÑO):</Label>
                    <InputRadio
                        type="radio"
                        id="1Prepa"
                        name="Pregunta1"
                        // value="1"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="1Prepa">1 Año</LabelRadio>

                    <InputRadio
                        type="radio"
                        id="2Prepa"
                        name="Pregunta1"
                        // value="2"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="2Prepa">2 Año</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="3Prepa"
                        name="Pregunta1"
                        // value="3"
                        Value={form.Pregunta1}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="3Prepa">3 Año</LabelRadio>
                </Div>

                {/* </Radio> */}
                <Label htmlFor="NombreUbicacion">Nombre Y Ubicación:
                <Input type="text"
                        id="NombreUbicacion"
                        name="NombreUbicacion"
                        Value={form.NombreUbicacion}
                        placeholder="Nombre Y Ubicación"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    {errors.NombreUbicacion && <p style={styles}>{errors.NombreUbicacion}</p>}

                </Label>
                <Label htmlFor="FechaIni">Fecha de Fin de Ciclo Escolar:
                <Input type="date"
                        id="FechaIni"
                        name="FechaIni"
                        Value={form.FechaIni}
                        placeholder="Fecha de Fin de Ciclo Escolar"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                </Label>

                <Label htmlFor="FechaFin">Fecha de Fin de Ciclo Escolar:
                <Input type="date"
                        id="FechaFin"
                        name="FechaFin"
                        Value={form.FechaFin}
                        placeholder="Fecha de Fin de Ciclo Escolar"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                </Label>

                <Label htmlFor="Grados">Grados:
                <Input type="text"
                        id="Grados"
                        name="Grados"
                        Value={form.Grados}
                        placeholder="Grados"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Grados && <p style={styles}>{errors.Grados}</p>}

                </Label>
                <Label htmlFor="Promedio">Promedio:
                <Input type="text"
                        id="Promedio"
                        name="Promedio"
                        placeholder="Promedio valido de 0 a 100"
                        Value={form.Promedio}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Promedio && <p style={styles}>{errors.Promedio}</p>}

                </Label>
                <Label htmlFor="CreditoEscolares">Crédito Escolares:
                <Input type="text"
                        id="CreditoEscolares"
                        name="CreditoEscolares"
                        placeholder="Crédito Escolares"
                        Value={form.CreditoEscolares}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.CreditoEscolares && <p style={styles}>{errors.CreditoEscolares}</p>}

                </Label>
                <Label htmlFor="Actividades">Actividades Extracurriculares:
                <Input type="text"
                        id="Actividades"
                        name="Actividades"
                        placeholder="Actividades Extracurriculares"
                        Value={form.Actividades}

                    />
                    {errors.Actividades && <p style={styles}>{errors.Actividades}</p>}

                </Label>
                <Label htmlFor="Trabajo">¿Trabajo De Medio Tiempo O Durante El Verano?
                <Input type="text"
                        id="Trabajo"
                        name="Trabajo"
                        Value={form.Trabajo}
                        placeholder="Su Trabajo De Medio Tiempo Durante El Verano"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                    {errors.Trabajo && <p style={styles}>{errors.Trabajo}</p>}

                </Label>

                <Formulario1>
                    <h3>Universidad</h3>

                </Formulario1>

                {/* <Radio> */}
                <Div>

                    <Label>Universidad (Semestres)</Label>
                    <InputRadio
                        type="radio"
                        id="0Universidad"
                        name="Pregunta2"
                        // value="0"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required 
                    />
                    <LabelRadio htmlFor="0Universidad">  0</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="1Universidad"
                        name="Pregunta2"
                        // value="1"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="1Universidad"> 1 </LabelRadio>
                    <InputRadio
                        type="radio"
                        id="2Universidad"
                        name="Pregunta2"
                        // value="2"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="2Universidad">2</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="3Universidad"
                        name="Pregunta2"
                        // value="3"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="3Universidad"> 3</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="4Universidad"
                        name="Pregunta2"
                        // value="4"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="4Universidad"> 4</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="5Universidad"
                        name="Pregunta2"
                        // value="5"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="5Universidad"> 5</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="6Universidad"
                        name="Pregunta2"
                        // value="6"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="6Universidad"> 6</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="7Universidad"
                        name="Pregunta2"
                        // value="7"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="7Universidad"> 7 </LabelRadio>
                    <InputRadio
                        type="radio"
                        id="8Universidad"
                        name="Pregunta2"
                        // value="8"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="8Universidad"> 8 </LabelRadio>
                    <InputRadio
                        type="radio"
                        id="9Universidad"
                        name="Pregunta2"
                        // value="9"

                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required />
                    <LabelRadio htmlFor="9Universidad"> 9 </LabelRadio>
                    <InputRadio
                        type="radio"
                        id="10Universidad"
                        name="Pregunta2"
                        // value="10"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="10Universidad"> 10</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="11Universidad"
                        name="Pregunta2"
                        // value="11"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="11Universidad"> 11</LabelRadio>
                    <InputRadio
                        type="radio"
                        id="12Universidad"
                        name="Pregunta2"
                        // value="12"
                        Value={form.Pregunta2}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    <LabelRadio htmlFor="12Universidad"> 12</LabelRadio>
                </Div>


                {/* <div id="OCULTAR"> */}
                {/* </Radio> */}
                <Label htmlFor="NombreUbicacionUni">Nombre Y Ubicación:
                <Input type="text"
                        id="NombreUbicacionUni"
                        name="NombreUbicacionUni"
                        placeholder="Nombre Y Ubicación"
                        Value={form.NombreUbicacionUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.NombreUbicacionUni && <p style={styles}>{errors.NombreUbicacionUni}</p>}

                </Label>
                <Label htmlFor="FechaIniUni">Fecha de Fin de Ciclo Escolar:
                <Input type="date"
                        id="FechaIniUni"
                        placeholder="Fecha de Fin de Ciclo Escolar"
                        name="FechaIniUni"
                        Value={form.FechaIniUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                </Label>

                <Label htmlFor="FechaFinUni">Fecha de Fin de Ciclo Escolar:
                <Input type="date"
                        id="FechaFinUni"
                        placeholder="Fecha de Fin de Ciclo Escolar"
                        name="FechaFinUni"
                        Value={form.FechaFinUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required

                    />
                </Label>

                <Label htmlFor="GradosUni">Grados:
                <Input type="text"
                        id="GradosUni"
                        placeholder="Grados"
                        name="GradosUni"
                        Value={form.GradosUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.GradosUni && <p style={styles}>{errors.GradosUni}</p>}

                </Label>

                <Label htmlFor="PromedioUni">Promedio:
                <Input type="text"
                        id="PromedioUni"
                        placeholder="Promedio valido de 0 a 100"
                        name="PromedioUni"
                        Value={form.PromedioUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.PromedioUni && <p style={styles}>{errors.PromedioUni}</p>}

                </Label>

                <Label htmlFor="CreditoEscolaresUni">Crédito Escolares:
                <Input type="text"
                        id="CreditoEscolaresUni"
                        placeholder="Crédito Escolares"
                        name="CreditoEscolaresUni"
                        Value={form.CreditoEscolaresUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.CreditoEscolaresUni && <p style={styles}>{errors.CreditoEscolaresUni}</p>}

                </Label>
                <Label htmlFor="ActividadesUni">Actividades Extracurriculares:
                <Input type="text"
                        id="ActividadesUni"
                        placeholder="Actividades Extracurriculares"
                        name="ActividadesUni"
                        Value={form.ActividadesUni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.ActividadesUni && <p style={styles}>{errors.ActividadesUni}</p>}

                </Label>

                <Label htmlFor="Pregunta1Uni">¿Qué Clases De Su Carrera Disfrutaba Más?
                <Input type="text"
                        id="Pregunta1Uni"
                        placeholder="Clase Que Mas Le Gustaba"
                        name="Pregunta1Uni"
                        Value={form.Pregunta1Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta1Uni && <p style={styles}>{errors.Pregunta1Uni}</p>}

                </Label>
                <Label htmlFor="Pregunta2Uni">¿Qué Clases De Su Carrera No Le Gustaban?
                <Input type="text"
                        id="Pregunta2Uni"
                        placeholder="Clase Que Menos Le Gustaba"
                        name="Pregunta2Uni"
                        Value={form.Pregunta2Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta2Uni && <p style={styles}>{errors.Pregunta2Uni}</p>}

                </Label>
                <Label htmlFor="Pregunta3Uni">¿Por Qué?
                <Input type="text"
                        id="Pregunta3Uni"
                        placeholder="Motivo Por El Cual No Le Gustaba"
                        name="Pregunta3Uni"
                        Value={form.Pregunta3Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta3Uni && <p style={styles}>{errors.Pregunta3Uni}</p>}

                </Label>
                <Label htmlFor="Pregunta4Uni">¿Cómo Financió Su Educación?
                <Input type="text"
                        id="Pregunta4Uni"
                        placeholder="Cómo Financió Su Educación"
                        name="Pregunta4Uni"
                        Value={form.Pregunta4Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta4Uni && <p style={styles}>{errors.Pregunta4Uni}</p>}

                </Label>
                <Label htmlFor="Pregunta5Uni">Trabajo Medio Tiempo O Durante El Verano:
                <Input type="text"
                        id="Pregunta5Uni"
                        placeholder="Su Trabajo De Medio Tiempo Durante El Verano"
                        name="Pregunta5Uni"
                        Value={form.Pregunta5Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta5Uni && <p style={styles}>{errors.Pregunta5Uni}</p>}

                </Label>
                <Label htmlFor="Pregunta6Uni">Otros Cursos, Seminario O Estudios:
                <Input type="text"
                        id="Pregunta6Uni"
                        placeholder="Cursos, Seminario O Estudios"
                        name="Pregunta6Uni"
                        Value={form.Pregunta6Uni}
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                    />
                    {errors.Pregunta6Uni && <p style={styles}>{errors.Pregunta6Uni}</p>}

                </Label>
                {/* </div> */}

                <ContenedorBotonCentrado>
                    <Link className="Anterior" to="/pagina2">Anterior</Link>
                    <Boton type="submit" value="Enviar">Siguiente</Boton>
                </ContenedorBotonCentrado>
            </Formulario>
        </>
    );
}



export default Educacion;