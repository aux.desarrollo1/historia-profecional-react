import styled, { css } from 'styled-components';
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const colores = {
	borde: "#0075FF",
	error: "#bb2929",
	exito: "#1ed12d"
}

const FormularioPreguntas = styled.form`
	text-align:center;
	display: grid;
	// grid-template-columns:1fr;
	// gap: 20px;
	// max-width: max-content;
	max-width:none;
	width: 70%;
	margin: auto;
	// padding: 80px;
`;
// const Main=styled.form`
// text-align:center;

// `;

const Formulario1 = styled.form`
text-align:center;
`;
const FormularioError = styled.form`
text-align:center;
fontWeight: "bold",
color: "#dc3545",
`;
const Radio = styled.form`
text-align:center;
`;

const Formulario = styled.form`
	display: grid;
	grid-template-columns: 1fr 1fr;
	gap: 20px;
	
	// text-align:center;
	display: grid;
	// grid-template-columns:1fr;
	// gap: 20px;
	// max-width: max-content;
	max-width:none;
	width: 80%;
	margin: auto;
	// padding: 80px;


	@media (max-width: 800px){
		grid-template-columns: 1fr;
	}
`;

const Input = styled.input`
	width: 100%;
	background: #fff;
	border-radius: 3px;
	height: 45px;
	line-height: 45px;
	padding: 0 40px 0 10px;
	transition: .3s ease all;
	border: 3px solid transparent;

	&:focus {
		border: 3px solid ${colores.borde};
		outline: none;
		box-shadow: 3px 0px 30px rgba(163,163,163, 0.4);
	}
`;

const Label = styled.label`
	font-weight: 700;
	display: block;
	// padding: 10px;
	min-height: 40px;
	cursor: pointer;
`;
const LabelPreguntas = styled.label`
	font-weight: 700;
	display: block;
	// padding: 10px;
	min-height: 40px;
	cursor: pointer;
`;
const LabelRadio = styled.label`
	font-weight: 700;
	// text-align: center;
	// display: block;
	padding: 10px;
	min-height: 20px;
	cursor: pointer;
`;

const GrupoInput = styled.div`
	position: relative;
	z-index: 90;
`;

const Div=styled.div`
 text-align:center;

`;

const InputTrabajo = styled.input`
	width: 5%;
	background: #fff;
	border-radius: 10px;
	height: 30px;
	margin-left:10px;
	line-height: 40px;
	padding: 0 40px 0 10px;
	transition: .3s ease all;
	 border: 3px solid transparent;
	&:focus {
		border: 3px solid ${colores.borde};
		outline: none;
		box-shadow: 3px 0px 30px rgba(163,163,163, 0.4);
	}
`;

const InputRadio = styled.input`
	width: 6%;
	background: #fff;
	border-radius: 3px;
	height: 25px;
	margin-left:10px;
	line-height: 40px;
	padding: 0 40px 0 10px;
	transition: .3s ease all;
	 border: 3px solid transparent;
	&:focus {
		border: 3px solid ${colores.borde};
		outline: none;
		box-shadow: 3px 0px 30px rgba(163,163,163, 0.4);
	}
`;


const LeyendaError = styled.p`
	font-size: 12px;
	margin-bottom: 0;
	color: ${colores.error};
	display: none;

	// ${props => props.valido === 'true' && css`
	// 	display: none;
	// `}

	// ${props => props.valido === 'false' && css`
	// 	display: block;
	`}
`;

const IconoValidacion = styled(FontAwesomeIcon)`
	position: absolute;
	right: 10px;
	bottom: 14px;
	z-index: 100;
	font-size: 16px;
	opacity: 0;

	${props => props.valido === 'false' && css`
		opacity: 1;
		color: ${colores.error};
	`}

	${props => props.valido === 'true' && css`
		opacity: 1;
		color: ${colores.exito};
	`}
`;


const ContenedorBotonCentrado = styled.div`
	display: flex;
	justify-content: center;
	// flex-direction: column;
	align-items: center;
	grid-column: span 2;
	text-align: center;
	margin-right:auto


	@media (max-width: 800px){
		grid-column: span 1;
	}
`;

const Boton = styled.button`
	height: 45px;
	line-height: 45px;
	width: 30%;
	background: #000;
	color: #fff;
	font-weight: bold;
	border: none;
	border-radius: 3px;
	cursor: pointer;
	transition: .1s ease all;

	&:hover {
		box-shadow: 3px 0px 30px rgba(163,163,163, 1);
	}
`;

// const LinK = styled.link`
// height: 45px;
// 	line-height: 45px;
// 	width: 30%;
// 	background: #000;
// 	color: #fff;
// 	font-weight: bold;
// 	border: none;
// 	border-radius: 3px;
// 	cursor: pointer;
// 	transition: .1s ease all;

// 	&:hover {
// 		box-shadow: 3px 0px 30px rgba(163,163,163, 1);
// 	}

// `;
// const ContenedorBotonCentrado2 = styled.div`
// 	display: flex;
// 	flex-direction: column;
// 	align-items: center;
// 	grid-column: span 2;

// 	@media (max-width: 800px){
// 		grid-column: span 1;
// 	}
// `;

// const Boton2 = styled.button`
// 	height: 45px;
// 	line-height: 45px;
// 	margin:10px;
// 	width: 30%;
// 	background: #000;
// 	color: #fff;
// 	font-weight: bold;
// 	border: none;
// 	border-radius: 3px;
// 	cursor: pointer;
// 	transition: .1s ease all;

// 	&:hover {
// 		box-shadow: 3px 0px 30px rgba(163,163,163, 1);
// 	}
// `;


const MensajeExito = styled.p`
	font-size: 14px;
	color: ${colores.exito};
`;

const MensajeError = styled.div`
	height: 45px;
	line-height: 45px;
	background: #F66060;
	padding: 0px 15px;
	border-radius: 3px;
	grid-column: span 2;
	p {
		margin: 0;
	} 
	b {
		margin-left: 10px;
	}
`;

export {
	Formulario,
	FormularioPreguntas,
	FormularioError,
	Label,
	LabelPreguntas,
	LabelRadio,
	GrupoInput,
	Input,
	InputRadio,
	InputTrabajo,
	Link,
	LeyendaError,
	IconoValidacion,
	ContenedorBotonCentrado,
	Boton,
	MensajeExito,
	MensajeError,
	Formulario1,
	Radio,
	Div
};