import { useForm } from "../componentes/validacionuno";
import React from "react";
import {
  Formulario,
  Label,
  Input,
  ContenedorBotonCentrado,
  Boton,
  Formulario1,
} from "../elementos/Formularios2";

const initialForm = {
  Paterno: "",
  Materno: "",
  Nombre: "",
  Direccion: "",
  Ciudad: "",
  Estado: "",
  Codigo: "",
  Telefono: "",
  Correo: "",
  Nacimiento: "",
  Civil: "",
  Puesto_Postular: "",
  Expectativa_Economica: ""

};

const validationsForm = (Formulario) => {
  let Errors = {};
  let Nombre = /^[a-zA-ZÀ-ÿ\s]{1,40}$/; // Letras y espacios, pueden llevar acentos.
  let Correo = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
  let Postal = /^\d{5,5}$/; // 7 a 14
  let Telefono = /^\d{7,10}$/; // 7 a 14
  let Generaal = /^.{1,20}$/;
  let letras = /^[A-Z]+$/i;
  let Lugar = /^[a-zA-Z0-9 ]+$/;

  if (!Formulario.Paterno.trim()) { } else if (!Nombre.test(Formulario.Paterno.trim())) {
    Errors.Paterno = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Materno.trim()) { } else if (!Nombre.test(Formulario.Materno.trim())) {
    Errors.Materno = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Nombre.trim()) { } else if (!Nombre.test(Formulario.Nombre.trim())) {
    Errors.Nombre = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Direccion.trim()) { } else if (!Lugar.test(Formulario.Direccion.trim())) {
    Errors.Direccion = "Sólo acepta letras y numeros.";
  }
  if (!Formulario.Ciudad.trim()) { } else if (!Nombre.test(Formulario.Ciudad.trim())) {
    Errors.Ciudad = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Estado.trim()) { } else if (!Nombre.test(Formulario.Estado.trim())) {
    Errors.Estado = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Codigo.trim()) { } else if (!Postal.test(Formulario.Codigo.trim())) {
    Errors.Codigo = "Sólo acepta 5 Numeros.";
  }
  if (!Formulario.Telefono.trim()) { } else if (!Telefono.test(Formulario.Telefono.trim())) {
    Errors.Telefono = "Sólo acepta 7 0 10 numeros.";
  }
  if (!Formulario.Correo.trim()) { } else if (!Correo.test(Formulario.Correo.trim())) {
    Errors.Correo = "correo123@correo.com";
  }
  if (!Formulario.Civil.trim()) { } else if (!letras.test(Formulario.Civil.trim())) {
    Errors.Civil = "Sólo acepta letras.";
  }
  if (!Formulario.Puesto_Postular.trim()) { } else if (!letras.test(Formulario.Puesto_Postular.trim())) {
    Errors.Puesto_Postular = "Sólo acepta letras y espacios en blanco.";
  }
  if (!Formulario.Expectativa_Economica.trim()) { } else if (!Generaal.test(Formulario.Expectativa_Economica.trim())) {
    Errors.Expectativa_Economica = "limite de caracteres aceptados.";
  }
  return Errors;
};

let styles = {
  fontWeight: "bold",
  color: "#dc3545",
};

const DatosPersonales = () => {
  const {
    form,
    errors,
    // loading,
    // response,
    handleChange,
    handleBlur,
    handleSubmit,
  } = useForm(initialForm, validationsForm);
  return (
    <>
      <Formulario1>
        <h2>Datos Personales</h2>
      </Formulario1>
      <Formulario  onSubmit={handleSubmit}>
        <Label htmlFor="Paterno">Paterno:
                    <Input type="text"
            id="Paterno"
            name="Paterno"
            Value={form.Paterno}
            placeholder="Apellido Paterno"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Paterno && <p style={styles}>{errors.Paterno}</p>}
        </Label>
        <Label htmlFor="Materno">Materno:
                    <Input type="text"
            id="Materno"
            name="Materno"
            Value={form.Materno}
            placeholder="Apellido Materno"

            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Materno && <p style={styles}>{errors.Materno}</p>}
        </Label>
        <Label htmlFor="Nombre">Nombre:
                    <Input type="text"
            id="Nombre"
            name="Nombre"
            Value={form.Nombre}
            placeholder="Nombre"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Nombre && <p style={styles}>{errors.Nombre}</p>}
        </Label>
        <Label htmlFor="Direccion De Domicilio">Dirección De Domicilio:
                    <Input type="text"
            id="Direccion"
            name="Direccion"
            Value={form.Direccion}
            placeholder="Dirección De Domicilio"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Direccion && <p style={styles}>{errors.Direccion}</p>}
        </Label>
        <Label htmlFor="Ciudad">Ciudad:
                    <Input type="text"
            id="Ciudad"
            name="Ciudad"
            Value={form.Ciudad}
            placeholder="Ciudad"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Ciudad && <p style={styles}>{errors.Ciudad}</p>}
        </Label>
        <Label htmlFor="Estado">Estado:
                    <Input type="text"
            id="Estado"
            name="Estado"
            Value={form.Estado}
            placeholder="Estado"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Estado && <p style={styles}>{errors.Estado}</p>}
        </Label>
        <Label htmlFor="Codigo Postal">Código Postal:
                    <Input type="text"
            id="Codigo"
            name="Codigo"
            Value={form.Codigo}
            placeholder="Código Postal"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Codigo && <p style={styles}>{errors.Codigo}</p>}
        </Label>
        <Label htmlFor="Telefono">Número Telefónico O Celular:
                    <Input type="num"
            id="Telefono"
            name="Telefono"
            Value={form.Telefono}
            placeholder="Número Telefónico O Celular"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Telefono && <p style={styles}>{errors.Telefono}</p>}
        </Label>
        <Label htmlFor="Direccion de correo electrónico">Dirección de correo electrónico:
                    <Input type="email"
            id="Correo"
            name="Correo"
            Value={form.Correo}
            placeholder="Correo@correo.com"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Correo && <p style={styles}>{errors.Correo}</p>}
        </Label>
        <Label htmlFor="Nacimiento">Fecha De Nacimiento:
                    <Input type="date"
            id="Nacimiento"
            name="Nacimiento"
            Value={form.Nacimiento}
            placeholder="Fecha De Nacimiento"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
        </Label>
        <Label htmlFor="Civil">Estado Civil:
                    <Input type="text"
            id="Civil"
            name="Civil"
            Value={form.Civil}
            placeholder="Estado Civil"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Civil && <p style={styles}>{errors.Civil}</p>}
        </Label>
        <Label htmlFor="Puesto Al Que Se Desea Postular">Puesto Al Que Se Desea Postular:
                    <Input type="text"
            id="Puesto_Postular"
            name="Puesto_Postular"
            Value={form.Puesto_Postular}
            placeholder="Puesto Postular"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Puesto_Postular && <p style={styles}>{errors.Puesto_Postular}</p>}
        </Label>
        <Label htmlFor="Expectativa Economica">Expectativa Economica:
                    <Input type="text"
            id="Expectativa_Economica"
            name="Expectativa_Economica"
            Value={form.Expectativa_Economica}
            placeholder="Expectativa Economica"
            onBlur={handleBlur}
            onChange={handleChange}
            required
          />
          {errors.Expectativa_Economica && <p style={styles}>{errors.Expectativa_Economica}</p>}
        </Label>
        <ContenedorBotonCentrado>
          <Boton type="submit" value="Enviar">Siguiente</Boton>
        </ContenedorBotonCentrado>
      </Formulario>
    </>
  );
};
export default DatosPersonales;