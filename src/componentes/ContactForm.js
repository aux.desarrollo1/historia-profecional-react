// import { useForm } from "../componentes/useForm";

// const initialForm = {
//   name: "",
// };

// const validationsForm = (form) => {
//   let errors = {};
//   let regexName = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/;
//   let regexEmail = /^(\w+[/./-]?){1,}@[a-z]+[/.]\w{2,}$/;
//   let regexComments = /^.{1,255}$/;

//   if (!form.name.trim()) {
//     errors.name = "El campo 'Nombre' es requerido";
//   } else if (!regexName.test(form.name.trim())) {
//     errors.name = "El campo sólo acepta letras y espacios en blanco";
//   }


//   return errors;
// };

// let styles = {
//   fontWeight: "bold",
//   color: "black",
// };

// const ContactForm = () => {
//   const {
//     form,
//     errors,
//     loading,
//     response,
//     handleChange,
//     handleBlur,
//     handleSubmit,
//   } = useForm(initialForm, validationsForm);

//   return (
//     <div>
//       <h2>Formulario de Contacto</h2>
//       <form onSubmit={handleSubmit}>
//         <input
//           type="text"
//           name="name"
//           placeholder="Escribe tu nombre"
//           onBlur={handleBlur}
//           onChange={handleChange}
//           value={form.name}
//           required
//         />
//         {errors.name && <p style={styles}>{errors.name}</p>}
        
//         {errors.comments && <p style={styles}>{errors.comments}</p>}
//         <input type="submit" value="Enviar" />
//       </form>
//     </div>
//   );
// };

// export default ContactForm;
